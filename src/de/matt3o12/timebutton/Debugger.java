package de.matt3o12.timebutton;

import java.util.ArrayList;
import java.util.LinkedHashSet;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * Sorry, English translation is missing because this class comes form a not public, old, plugin.
 * I'm going to translate the class when i have time.
 * 
 * @author Matt3o12
 */
public class Debugger {
	/** Liste mit allen Spielern, die den Debuger entfangen wollen. */
	private static LinkedHashSet<CommandSender> listener = new LinkedHashSet<CommandSender>();
	
	/** Die Senders, f�r den der Debuger gilt. */
	private ArrayList<CommandSender> senders;
	
	/**
	 * Ein neuer Debuger f�r sender
	 * @param sender Der sender, der die Nachrichten entfangen soll.
	 */
	public Debugger ( CommandSender sender ) {
		senders = new ArrayList<CommandSender>();
		
		senders.add( sender );
	}
	
	public Debugger ( CommandSender[] senders ){
		this.senders = new ArrayList<CommandSender>();
		
		for ( CommandSender sender : senders ){
			this.senders.add( sender );
		}
	}

	/**
	 * F�gt einen neuen Player zum Debuger hinzu.
	 * @param sender
	 */
	public static void add ( CommandSender sender ) {
		listener.add( sender );
	}
	
	/**
	 * Entfehrnt einen Player aus den Debuger.
	 * @param sender
	 */
	public static void remove (CommandSender sender){
		listener.remove( sender );
	}
	
	/**
	 * Eine neue Error Message.
	 * 
	 * Wichtig: Wird nur gesendet, wenn der Player im debuger ist.
	 * @param message
	 */
	public void error(Object message, Object...args) {
		send( ChatColor.RED + message.toString() );
	}
	
	/**
	 * Eine neue Warning Message.
	 * 
	 * Wichtig: Wird nur gesendet, wenn der Player im debuger ist.
	 * @param message
	 */
	public void warn(Object message, Object...args) {
		send( ChatColor.YELLOW + message.toString() );
	}
	
	/**
	 * Eine neue Info Message.
	 * 
	 * Wichtig: Wird nur gesendet, wenn der Player im debuger ist.
	 * @param message
	 */
	public void info(Object message, Object... args) {
		send(ChatColor.GRAY + message.toString(), args);
	}
	
	/**
	 * Eine neue "gute" Nachricht.
	 * 
	 * Wichtig: Wird nur gesendet, wenn der Player im debuger ist.
	 * @param message
	 */
	public void good(Object message, Object...args) {
		send(ChatColor.GREEN + message.toString(), args);
	}
	
	private void send(Object message, Object... args) {
		message = "[TBUTTON-DEBUG]" + message;
		String formatedMessage = String.format(message.toString(), args);
		for (CommandSender sender : senders){
			if (!isInListener(sender))
				return;
						
			sender.sendMessage(formatedMessage);
		}
	}
	
	/**
	 * Wird verwendet, wenn eine Exception geworfen wird, und der Player, vorausgesetzt er ist im Debuger davon erf�hrt.
	 * @param e Die Exception
	 */
	public void sendException ( Throwable e ){
		for ( CommandSender sender : senders ){
			if ( !isInListener( sender ) )
				return;
			
			sender.sendMessage( ChatColor.RED + e.toString() );
			StackTraceElement[] stacks = e.getStackTrace();
			for ( int i = 0; stacks.length > i; i++){
				if ( i == 5 ){
					error( Integer.toString( ( i-20 ) ) + "Weitere" );
					break;
				}
				sender.sendMessage( ChatColor.RED + "   " + stacks[i].toString() );
			}
		}		
	}
	
	/**
	 * Gibt zur�ck ob der Player der Aktuellen Instance im Debuger ist.
	 * Ist allerdings nichts anderes als: <code>Debug.isInListener(sender)</code>
	 * 
	 * @see #isInListener(CommandSender)
	 * @deprecated Use: {@link Debugger#isInListener(CommandSender)}
	 */
	@Deprecated
	public boolean isDebuggerActiv(){
		return false;
	}
	
	/**
	 * Gibt zur�ck ob sender Nachrichten vom Debuger entfangen will
	 * @param sender Der sender, der nachrichten empfangen (will)
	 * @return Ob der sender nachrichten entpfangen will.
	 */
	public static boolean isInListener( CommandSender sender ) {
		return listener.contains( sender );
	}
}
