package de.matt3o12.timebutton.events;

import static de.matt3o12.utils.Language._;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.matt3o12.timebutton.ClickCountButton;
import de.matt3o12.timebutton.Debugger;

public class TBAddButtonEvent extends TBEvent {
	private Block button;
	private CommandSender player;
	private int clickCount;
	private int resetTime;

	public TBAddButtonEvent(Player player, Block button, int clickCount, int resetTime) {
		super();
		this.player = player;
		this.button = button;
		this.clickCount = clickCount;
		this.resetTime = resetTime;
		setCancelled(true); //All add and remove events should cancel the bukkit event, too.
	}
	
	@Override
	public void execute() {
		Debugger debugger = new Debugger(player);
		if (button.getType() != Material.STONE_BUTTON){
			player.sendMessage(ChatColor.RED +_("buttonTouch.notButton", "Please click a button"));
			debugger.info("Click button: %s (Type: %s)", button, button.getType());
			return;
		}

		if (!ClickCountButton.isBlockCountedButton(button)){
			new ClickCountButton(button, clickCount, resetTime);
			
			player.sendMessage(ChatColor.GREEN + _("buttonAdd.successful", "The button has been added successful"));
			debugger.info("%s has been successful added.", button);
		} else {
			player.sendMessage(ChatColor.RED + _("buttonAdd.exists", "The button is already a counted button exists."));
			debugger.info("ClickCountButton.isBlockCountedButton(block) returns false");
		}
	}
}
