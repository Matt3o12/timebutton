package de.matt3o12.timebutton.events;

import static de.matt3o12.utils.Language._;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import de.matt3o12.timebutton.ClickCountButton;
import de.matt3o12.timebutton.Debugger;
import de.matt3o12.timebutton.TimeButton;

public class TBClickCountButtonEvent extends TBEvent {
	private Block block;
	private Player player;
	private boolean leftClick;

	public TBClickCountButtonEvent(Player player, Block block, boolean leftClick) {
		super();
		this.player = player;
		this.block = block;
		this.leftClick = leftClick;
	}

	@Override
	public void execute() {
		Debugger debugger = new Debugger(player);
		if (leftClick && !TimeButton.shouldHandleLeftClickOnButtons){
			debugger.warn("Left clicks are ignore since minecraft 1.4.2");
			return;
		}
		
		ClickCountButton cbutton = ClickCountButton.getCountedButton(block);
		String m;
		if (cbutton.canPlayerClickButton(player)) {
			cbutton.playerClickedButton(player);
			List<Long> clicksForPlayer = cbutton.getClicksForPlayer(player);
			int clicksLeft = cbutton.getClickCount() - clicksForPlayer.size();
			if (clicksLeft == 0) {
				int nextClick = cbutton.getNextClick(player) / 1000;
				m = _("buttonTouch.lastClick",
						"This was your last click for this button for the next %s secounds.");
				m = String.format(m, nextClick);
			} else {
				m = _("buttonTouch.clicksLeft", "You have %s clicks left.");
				m = String.format(m, clicksLeft);
			}
		} else {
			int nextClick = cbutton.getNextClick(player) / 1000;
			m = _("buttonTouch.cancelled",
					"Sorry. Click limit achieved. Next click in %s secounds");
			m = String.format(ChatColor.YELLOW + m, nextClick);
			setCancelled(true);
		}
		player.sendMessage(m);
	}
}
