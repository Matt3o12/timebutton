package de.matt3o12.timebutton.events;

import static de.matt3o12.utils.Language._;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import de.matt3o12.timebutton.ClickCountButton;

public class TBRemoveButtonEvent extends TBEvent {
	private Block block;
	private Player player;

	public TBRemoveButtonEvent(Block block, Player player) {
		super();
		this.block = block;
		this.player = player;
	}

	@Override
	public void execute() {
		if (ClickCountButton.isBlockCountedButton(block)) {
			ClickCountButton cButton = ClickCountButton.getCountedButton(block);
			cButton.removeButton();
			player.sendMessage(ChatColor.GREEN
					+ _("buttonRemove.success",
							"The counted button is now a normal button."));
		} else {
			player.sendMessage(ChatColor.RED
					+ _("buttonRemove.noCountedButton",
							"Please click to a counted button."));
		}
	}

}
