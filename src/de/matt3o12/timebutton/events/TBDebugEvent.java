package de.matt3o12.timebutton.events;

/**
 * All event which only use to send debug messages should extends the class.
 * The class has no effect, yet.
 * 
 * @author Matt3o12
 */
public abstract class TBDebugEvent extends TBEvent {

}
