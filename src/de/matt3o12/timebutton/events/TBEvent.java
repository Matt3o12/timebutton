package de.matt3o12.timebutton.events;


/**
 * Classes which implements TBEvent are TimeButton event. TBEvents are usually called by Listeners.
 * Those events should execute the code what player want to do.
 * For example {@link TBAddButtonEvent} should be called if a player is going to add a button.
 * 
 * @author Matt3o12
 *
 */
public abstract class TBEvent {
	private boolean cancelled;

	/**
	 * This interface is needed when TBEvent subclass able to cancel events.
	 * It can occur that TBEvents must cancel the current event. For example, it can occur 
	 * by a {@link TBClickCountButtonEvent} when the user are not allowed to click the button.
	 * 
	 * Note for TBEvents subclasses:
	 * 	Event should not need a bukkit event ({@link org.bukkit.event.Event}.
	 */
	public interface ShouldCancelEventCallback{
		/**
		 * TBEvent subclasses will call the method if an unexpected error occur
		 * and the event have to be cancelled.   
		 */
		public void shouldCancelEvent();
	}
	
	/**
	 * Init a TBEvent. If you override construct you have to 
	 * add the <code>super()</code> function.
	 */
	public TBEvent() {
		setCancelled(false);
	}
	
	/**
	 * The method will return if the event should be cancelled.
	 * @return
	 */
	public boolean isCancelled(){
		return cancelled;
	}
	
	/**
	 * Set the event cancelled or not.
	 * 
	 * The method should only called directly by subclasses.
	 * You shouln'd override this method.
	 * @param cancelled True if the bukkit event should be cancelled.
	 */
	protected void setCancelled(boolean cancelled){
		this.cancelled = cancelled;
	}
	
	/**
	 * You have to override the method. 
	 * The event shouln't work, run, execute or whatever in construct.
	 * The main code have to run here!
	 */
	public abstract void execute();
}
