package de.matt3o12.timebutton.events;

import org.bukkit.entity.Player;

import de.matt3o12.timebutton.Debugger;

public class TBNoticeThatStoneButtonNotCountedButtonEvent extends TBDebugEvent {
	private Debugger debugger;

	public TBNoticeThatStoneButtonNotCountedButtonEvent(Player player) {
		this.debugger = new Debugger(player);
	}

	@Override
	public void execute() {
		debugger.info("PlayerAction != ADD_BLOCK"
				+ " and ClickCountButton.isBlockCountedButton(Block) is false");
	}

}
