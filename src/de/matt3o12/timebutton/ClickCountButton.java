package de.matt3o12.timebutton;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import de.matt3o12.utils.Util;

/**
 * Represents a button which represents counted button.
 * Counten buttons are the core if of the plugin.<br><br>
 * The class has all important classes to if a player 
 * can click a button or when can the player click next.
 *  
 * @author Matt3o12
 */
public class ClickCountButton {
	private static File saveFile;
	private static YamlConfiguration save;
	private Block theButton;
	private int clickCount, resetTime;
	private boolean removed = false;
	
	/**
	 * Create a new counted button.
	 * 
	 * @param theButton The button new counted button.
	 * @param clickCount Max clicks for the button.
	 * @param resetTime The time when the click should be reseted.
	 */
	public ClickCountButton(Block theButton, int clickCount, int resetTime) {
		if (theButton.getType() != Material.STONE_BUTTON)
			throw new RuntimeException("The block: " + theButton.toString()
					+ " isn´t a stone button.");

		this.theButton = theButton;
		this.clickCount = clickCount;
		this.resetTime = resetTime;	
		save();
	}
	
	/**
	 * "Load" a saved button.
	 * 
	 * @param theBlock The button who should loaded.
	 */
	private ClickCountButton(Block theBlock){
		if (theBlock.getType() != Material.STONE_BUTTON)
			throw new RuntimeException("The block: " + theBlock.toString()
					+ " isn´t a stone button.");

		this.theButton = theBlock;
		String id = getButtonNode();
		clickCount = save.getInt(id + ".maxClickCount");
		resetTime = save.getInt(id + ".resetTime");
	}

	/**
	 * Call that, after the counted button clicked.<br>
	 * 
	 * @param player
	 */
	public void playerClickedButton(Player player){
		if (removed)
			return;
		
		ArrayList<Long> clicks = new ArrayList<Long>();
		clicks.addAll(getClicksForPlayer(player));
		clicks.add(System.currentTimeMillis());
		setClicksForPlayer(player, clicks);
		saveToDisc();
	}
	
	/**
	 * Return if player can click the button.
	 * 
	 * @param player The player who wants to click.
	 * @return If the player allowed to click.
	 */
	public boolean canPlayerClickButton(Player player){
		return getClicksForPlayer(player).size() < clickCount;
	}
	
	/**
	 * Return a list with all not elapsed clicks for the player.
	 * 
	 * @param player The player.
	 * @return A list with {@link Long} values.
	 */
	public List<Long> getClicksForPlayer(Player player){
		if (removed)
			return null;
		
		String clicksNode = String.format("%s.clicks.%s", getButtonNode(), player.getName()); 
		List<Long> result = save.getLongList(clicksNode);
		return result != null ? clearList(result) : new ArrayList<Long>();
	}

	/**
	 * Return the time when the player can click.
	 * 
	 * @param player The player who wants to click.
	 * @return Time in milliseconds
	 */
	public int getNextClick(Player player){
		if (removed)
			return 0;
		
		List<Long> clicks = getClicksForPlayer(player);
		long nextC = clicks.get(0) + (getResetTime() * 1000) - System.currentTimeMillis();
		return (int)nextC;
	}

	/**
	 * Return the click count.
	 * 
	 * @return The click count.
	 */
	public int getClickCount(){
		if (removed)
			return -1;
		return clickCount;
	}

	/**
	 * Return the reset time.
	 * 
	 * @return The reset time.
	 */
	public int getResetTime() {
		if (removed)
			return -1;
		
		return resetTime;
	}

	public void removeButton() {
		if (removed)
			return;
		
		save.set(getButtonNode(), null);
		saveToDisc();
		removed  = true;
	}

	/**
	 * Return if theBlock a counted button.
	 * 
	 * @param theBlock The block.
	 * @return If the block a counted button.
	 */
	public static boolean isBlockCountedButton(Block theBlock){
		String blockNode = String.format("block.%s", generateID(theBlock));
		if (save.isSet(blockNode)){
			if (theBlock.getType() == Material.STONE_BUTTON) {
				return true;
			} else {
				save.set(blockNode, null);
				saveToDisc();
				return false;
			}
		} else {
			return false;
		}
	}
	
	/**
	 * Return the counted button.<br>
	 * 
	 * @param theBlock The block.
	 * @return null if {@link #isBlockCountedButton(Block)} returns false.
	 */
	public static ClickCountButton getCountedButton(Block theBlock){
		if (isBlockCountedButton(theBlock))
			return new ClickCountButton(theBlock);
		
		return null;
	}

	/**
	 * Save the "save" instance variable to the disc.<br>
	 * You shouldn't override the methode.   
	 */
	protected static void saveToDisc(){
		try {
			save.save(saveFile);
		} catch (IOException e) {
			e.printStackTrace();
			Debugger d = new Debugger(Bukkit.getOnlinePlayers());
			d.error("Can not save buttons.");
			d.sendException(e);
		}		
	}

	/**
	 * Generate a identifier for the Block.<br>
	 * <br>
	 * Example for the block with position: x = 3, y = 6, z = -9<br>
	 * x3y6z-9
	 * 
	 * @return The ID for the block.
	 */
	private static String generateID(Block block){
		return String.format("x%sy%sz%s", block.getX(), block.getY(), block.getZ());
	}
	
	static {
		File savePath = new File(Util.joinFilePath("plugins", "TimeButton").toString());
		if (!savePath.isDirectory())
			savePath.mkdirs();
		
		saveFile = new File(savePath, "buttons.yml");
		save = YamlConfiguration.loadConfiguration(saveFile);
	}

	/**
	 * Return the button node.
	 * 
	 * @return "block.{@link #getButtonNode()}"
	 */
	private String getButtonNode(){
		return String.format("block.%s", getButtonID());
	}

	/**
	 * Return the button id.
	 * 
	 * @return {@link #generateID(Block)};
	 */
	private String getButtonID(){
		return generateID(theButton);
	}

	/**
	 * Set clicks for a player.
	 *
	 * @param clicks A {@link List} with the new clicks.
	 */
	private void setClicksForPlayer(Player player, List<Long> clicks){
		String clicksNode = String.format("%s.clicks.%s", getButtonNode(), player.getName());
		save.set(clicksNode, clicks);
		saveToDisc();
	}

	/**
	 * Remove the elapsed time.
	 * 
	 * @param values The old values.
	 * @return The List without the elapsed values. 
	 */
	private List<Long> clearList(List<Long> values) {
		ArrayList<Long> clearedList = new ArrayList<Long>();
		for (Long theValue : values) {
			 if (!isTimeExpired(theValue))
				 clearedList.add(theValue);
		}
	
		return clearedList;
	}

	/**
	 * Returns if the time expired/reseted.
	 * 
	 * @param time The time
	 * @return true if reseted/expired
	 */
	private boolean isTimeExpired(Long time){
		if (System.currentTimeMillis() >= time + (resetTime * 1000))
			return true;
		
		return false;
	}

	/**
	 * Save the block position, clickcount and reset time to the disc.
	 * 
	 * @see #saveToDisc()
	 */
	private void save(){
		String id = getButtonNode();
		save.set(id + ".x", theButton.getX());
		save.set(id + ".y", theButton.getY());
		save.set(id + ".z", theButton.getZ());
		save.set(id + ".maxClickCount", clickCount);
		save.set(id + ".resetTime", resetTime);
		saveToDisc();
	}
}
