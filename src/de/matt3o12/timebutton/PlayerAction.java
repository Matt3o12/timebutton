package de.matt3o12.timebutton;

import java.util.HashMap;

/**
 * {@link PlayerAction} contains method to check what a play want to do.
 * This contains the {@link ActionType} and perhaps properties about the action.
 * 
 * Properties for a action are attributes for a action. 
 * For example: What is the click count for the added button.
 * 
 * @author Matt3o12
 */
public class PlayerAction {
	private HashMap<? extends Object, ? extends Object> properties;
	private ActionType actionType;
	
	/**
	 * All actions types, yet.
	 * 
	 * @author Matt3o12
	 */
	public enum ActionType{
		NO_ACTION, ADD_BUTTON, REMOVE_BUTTON;
	}
	
	/**
	 * Create a new action without properties.
	 * 
	 * The method class {@link PlayerAction#PlayerAction(ActionType, HashMap)}
	 * with <code>null</code> as Hashmap.
	 * 
	 * @param actionType The action what the player is going to do.
	 * @see PlayerAction#PlayerAction(ActionType, HashMap)
	 */
	public PlayerAction(ActionType actionType){
		this(actionType, null);
	}
	
	/**
	 * Create a action with properties.
	 * 
	 * @param actionType The action what the player is going to do.
	 * @param properties Properties about the action.
	 */
	public PlayerAction(ActionType actionType, HashMap<? extends Object, ? extends Object> properties){
		setProperties(properties);
		setActionType(actionType);
	}
	
	/**
	 * Gets the properties.
	 * 
	 * @return The properties.
	 */
	public HashMap<? extends Object, ? extends Object> getProperties(){
		return properties;
	}
	
	/**
	 * Set properties.
	 * 
	 * @param properties The new properties.
	 */
	public void setProperties(HashMap<? extends Object, ? extends Object> properties){
		this.properties = properties;
	}

	/**
	 * The action type.
	 * 
	 * @return
	 */
	public ActionType getActionType() {
		return actionType;
	}

	private void setActionType(ActionType actionType) {
		this.actionType = actionType;
	}
}
