package de.matt3o12.timebutton.command;

import org.bukkit.permissions.PermissionDefault;

import de.matt3o12.commands.AbstractHelpPage;
import de.matt3o12.timebutton.TimeButton;

import static de.matt3o12.utils.Language._;

public class HelpCommand extends AbstractHelpPage {

	public HelpCommand(TimeButton plugin) {
		super(plugin);

		this.setName(_("helpCommand.name", "Help"));
		this.setCommandUsage(_("helpCommand.usage", "/timebutton help"));
		this.setCommandDesc(_("helpCommand.desc",
				"Appears help for all commands."));
		this.addKey("timebutton", 0, 0);
		this.addKey("timebutton help", 0, 1);
		this.addCommandExample(_("helpCommand.example1",
				"/help - Appears the time button help menu."));
		this.setPermission("", "", PermissionDefault.NOT_OP);

		this.addCommand(new AddButtonCommand(plugin));
		this.addCommand(new RemoveButtonCommand(plugin));
	}
}
