package de.matt3o12.timebutton.command;

import static de.matt3o12.utils.Language._;

import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import de.matt3o12.commands.CommandType;
import de.matt3o12.timebutton.PlayerAction;
import de.matt3o12.timebutton.PlayerAction.ActionType;
import de.matt3o12.timebutton.TimeButton;

public class AddButtonCommand extends TimeButtonCommand{
	public AddButtonCommand(TimeButton plugin) {
		super(plugin);
		setName(_("addButtonCommand.name", "Add Button"));
		setArgRange(2, 2);
		setCommandUsage(_("addButtonCommand.usage", "/timebutton add <max touches> <reset time>"));
		setPermission("timebutton.add", "Allowned the add command", PermissionDefault.OP);
		setCommandDesc(_("addButtonCommand.desc", "Add a button with a click count. Use the reset time in minutes"));
		setCommandType(CommandType.OPERATOR_COMMAND);
		addCommandExample(_("addButtonCommand.example1", "/tb add 1 1440 - Add a reset button."));
		addKey("timebutton add");
	}
	
	@Override
	public void runCommand(CommandSender sender, List<String> args) {
		if (!(sender instanceof Player)){
			sender.sendMessage(ChatColor.RED + _("doAsPlayer", "Please do that as Player"));
			return;
		}
		
		int clickCount, resetTime;
		try{
			clickCount = Integer.parseInt(args.get(0));
			resetTime = Integer.parseInt(args.get(1)) * 60;
		} catch (NumberFormatException e) {
			sender.sendMessage(ChatColor.RED + _("addButtonCommand.clickCountOrResetTimeNotNumber", "Click count or reset time is not a number"));
			return;
		}
		
		Player thePlayer = (Player) sender;
		HashMap<String, Integer> actionInfos = new HashMap<String, Integer>();
		actionInfos.put("clickCount", clickCount);
		actionInfos.put("resetTime", resetTime);

		PlayerAction action = new PlayerAction(ActionType.ADD_BUTTON, actionInfos);
		plugin.getPlayerManager().setActionForPlayer(thePlayer, action);
		
		thePlayer.sendMessage(ChatColor.YELLOW + _("addButtonCommand.touchButton", "Please touch a button."));
	}
}
