/**
 * 
 */
package de.matt3o12.timebutton.command;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import de.matt3o12.commands.CommandType;
import de.matt3o12.timebutton.Debugger;
import de.matt3o12.timebutton.TimeButton;

/**
 * @author Matt3o12
 *
 */
public class DebugCommand extends TimeButtonCommand {

	/**
	 * @param plugin
	 */
	public DebugCommand(TimeButton plugin) {
		super(plugin);
		setCommandUsage("/timebutton -debug");
		setCommandDesc("The Debug command - ONLY for developers!");
		setCommandType(CommandType.DEVELOPER_COMMAND);
		setArgRange(0, 0);
		setName("Debug");
		setPermission("timebutton.debug", "Only for developers!", PermissionDefault.OP);
		addKey("timebutton -debug");
	}

	@Override
	public void runCommand(CommandSender sender, List<String> args) {
		if (!(sender instanceof Player)){
			sender.sendMessage(ChatColor.RED + "Please do that as Player.");
			return;
		}
		
		if (!Debugger.isInListener(sender)){
			Debugger.add(sender);
			sender.sendMessage(ChatColor.GREEN + "You are added to the debugger.");
		} else {
			Debugger.remove(sender);
			sender.sendMessage(ChatColor.GREEN + "You are removed from the debugger.");
		}
	}

}
