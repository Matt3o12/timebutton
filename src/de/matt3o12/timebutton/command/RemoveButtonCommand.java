package de.matt3o12.timebutton.command;

import static de.matt3o12.utils.Language._;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import de.matt3o12.commands.CommandType;
import de.matt3o12.timebutton.PlayerAction.ActionType;
import de.matt3o12.timebutton.TimeButton;

public class RemoveButtonCommand extends TimeButtonCommand {

	public RemoveButtonCommand(TimeButton plugin) {
		super(plugin);
		setName(_("removeButtonCommand.name", "remove a counted button"));
		setArgRange(0, 0);
		setCommandUsage(_("removeButtonCommand.usage", "/timebutton timebutton"));
		setPermission("timebutton.remove", "Allowned the remove command", PermissionDefault.OP);
		setCommandDesc(_("removeButtonCommand.desc", "Remove a counted button."));
		setCommandType(CommandType.OPERATOR_COMMAND);
		addCommandExample(_("removeButtonCommand.example1", "/tb remove - Remove a counted button."));
		addKey("timebutton remove");
	}

	@Override
	public void runCommand(CommandSender sender, List<String> args) {
		if (!(sender instanceof Player)){
			sender.sendMessage(ChatColor.RED + _("doAsPlayer", "Please do that as Player"));
			return;
		}
		
		Player player = (Player) sender;
		plugin.getPlayerManager().setActionForPlayer(player, ActionType.REMOVE_BUTTON);
		player.sendMessage(ChatColor.YELLOW + _("removeButtonCommand.touchBlock", "Please touch a counted button."));
	}

}
