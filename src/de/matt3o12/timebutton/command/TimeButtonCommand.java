package de.matt3o12.timebutton.command;

import de.matt3o12.commands.CommandType;
import de.matt3o12.commands.Matt3o12Command;
import de.matt3o12.commands.SimpleCommand;
import de.matt3o12.timebutton.TimeButton;

abstract public class TimeButtonCommand extends Matt3o12Command implements SimpleCommand {
	static{
		addAlias("timebutton", "tb");
		addAlias("timebutton", "tbutton");
	}
	
	protected TimeButton plugin;
	
	public TimeButtonCommand(TimeButton plugin) {
		super(plugin);
		
		this.plugin = plugin;
		setCommandType(CommandType.USER_COMMAND);
	}
}
