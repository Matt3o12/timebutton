/**
 * 
 */
package de.matt3o12.timebutton;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import com.pneumaticraft.commandhandler.CommandHandler;

import de.matt3o12.timebutton.command.AddButtonCommand;
import de.matt3o12.timebutton.command.DebugCommand;
import de.matt3o12.timebutton.command.HelpCommand;
import de.matt3o12.timebutton.command.RemoveButtonCommand;
import de.matt3o12.timebutton.listeners.PlayerClickListener;
import de.matt3o12.utils.Language;
import de.matt3o12.utils.PermissionInterface;
import de.matt3o12.utils.Util;

/**
 * The main class of the project. The project is starting here.
 * @author Matt3o12
 */
public class TimeButton extends JavaPlugin {
	public static boolean shouldHandleLeftClickOnButtons = true;
	private static File pluginFolder;
	private CommandHandler commandHandler;
	private PlayerManager playerManager;
	
	static{
		pluginFolder = new File(Util.joinFilePath("plugins", "TimeButton").toString());
		if (!pluginFolder.isDirectory())
			pluginFolder.mkdir();
	}
	
	@Override
	public void onEnable() {
		super.onEnable();
		
		Locale recommandLocal = new Locale(getConfig().getString("lang", "en"));
		Locale.setDefault(recommandLocal);
		Language.setDefaultLanguage(recommandLocal); // Fix for issues #3
		
		commandHandler = new CommandHandler(this, new PermissionInterface());
		playerManager = new PlayerManager();
		Bukkit.getPluginManager().registerEvents(new PlayerClickListener(this), this);
		initCommands();
		initConfig();
		
		// Fix for issues #2.
		// I know, well-written code doesn't look so but I havn't a better idea.
		String mcVersionString = Bukkit.getBukkitVersion().substring(0, 4);
		if (mcVersionString.endsWith("."))
			mcVersionString = mcVersionString.substring(0, 3);
		String[] mcVersion = mcVersionString.split("\\."); 
		if (Integer.parseInt(mcVersion[0]) > 1)
			shouldHandleLeftClickOnButtons = false;
		else if (Integer.parseInt(mcVersion[1]) >= 4)
			shouldHandleLeftClickOnButtons = false;
	}
	
	@Override
	public void onDisable() {
		super.onDisable();
	}
	
	private void initConfig() {
		try {
			File config, defaultConfig;
			config = new File(Util.joinFilePath("plugins", "TimeButton", "config.yml").toString());
			defaultConfig = new File(Util.joinFilePath("plugins", "TimeButton", "defaultConfig.yml").toString());
			InputStream stream = null;
			if (!config.isFile()){
				config.createNewFile();
				stream = getClass().getClassLoader().getResourceAsStream("defaultConfig.yml");
				Util.copyInputStreamToFile(stream, config);
				stream.close();
			}
			
			if (!defaultConfig.isFile()){
				config.createNewFile();
				stream = getClass().getClassLoader().getResourceAsStream("defaultConfig.yml");
				Util.copyInputStreamToFile(stream, defaultConfig);
				stream.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void initCommands() {
		commandHandler.registerCommand(new HelpCommand(this));
		commandHandler.registerCommand(new AddButtonCommand(this));
		commandHandler.registerCommand(new RemoveButtonCommand(this));
		commandHandler.registerCommand(new DebugCommand(this));
	}
	
	@Override
	public boolean onCommand( CommandSender sender, Command command, String label, String[] args ){
		try{
	       if (!this.isEnabled()) {
	            sender.sendMessage("This plugin is Disabled!");
	            return true;
	        }
	       
	       ArrayList<String> allArgs = new ArrayList<String>(Arrays.asList(args));
	       allArgs.add(0, command.getName());
                
        	return commandHandler.locateAndRunCommand(sender, allArgs);
		} catch ( Exception e ){
			e.printStackTrace();
			return false;
		}
	}


	public PlayerManager getPlayerManager() {
		return playerManager; 
	}
}
