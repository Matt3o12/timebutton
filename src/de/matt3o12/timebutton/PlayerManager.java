package de.matt3o12.timebutton;

import java.util.HashMap;

import org.bukkit.entity.Player;

import de.matt3o12.timebutton.PlayerAction.ActionType;

/**
 * The manager for all player interacts (actions).
 * 
 * @author Matt3o12
 */
public class PlayerManager {
	private HashMap<Player, PlayerAction> playerActions;
	
	public PlayerManager() {
		playerActions = new HashMap<Player, PlayerAction>();
	}
	
	/**
	 * Gets the action for the player.
	 * 
	 * @return The action for player.
	 */
	public PlayerAction getActionForPlayer(Player player){
		if (!playerActions.containsKey(player))
			return new PlayerAction(ActionType.NO_ACTION);
		else
			return playerActions.get(player);
	}
	
	/**
	 * Sets the action for {@link Player}.<br>
	 * This method creates a {@link PlayerAction} instance 
	 * and calls {@link PlayerManager#setActionForPlayer(Player, PlayerAction)}
	 * 
	 * @param player The {@link Player}.
	 * @param action The {@link PlayerAction} which {@link Player} is going to do.
	 * @see PlayerManager#setActionForPlayer(Player, PlayerAction)
	 */
	public void setActionForPlayer(Player player, ActionType action){
		setActionForPlayer(player, new PlayerAction(action));
	}
	
	/**
	 * Set the action for {@link Player} with or without properties.
	 * 
	 * @param player The {@link Player}
	 * @param action The {@link PlayerAction} which {@link Player} is going to do.
	 */
	public void setActionForPlayer(Player player, PlayerAction action){
		ActionType actionType = action.getActionType();
		if ((actionType == ActionType.NO_ACTION) && playerActions.containsKey(player))
			playerActions.remove(player);
		else if (actionType != ActionType.NO_ACTION)
			playerActions.put(player, action);
	}
}
