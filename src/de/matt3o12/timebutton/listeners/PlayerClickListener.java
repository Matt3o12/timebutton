package de.matt3o12.timebutton.listeners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import de.matt3o12.timebutton.ClickCountButton;
import de.matt3o12.timebutton.PlayerAction;
import de.matt3o12.timebutton.PlayerAction.ActionType;
import de.matt3o12.timebutton.PlayerManager;
import de.matt3o12.timebutton.TimeButton;
import de.matt3o12.timebutton.events.TBAddButtonEvent;
import de.matt3o12.timebutton.events.TBClickCountButtonEvent;
import de.matt3o12.timebutton.events.TBEvent;
import de.matt3o12.timebutton.events.TBNoticeThatStoneButtonNotCountedButtonEvent;
import de.matt3o12.timebutton.events.TBRemoveButtonEvent;


/**
 * The listeners for all player interacts.
 */
public class PlayerClickListener implements Listener {
	private TimeButton plugin;
	
	public PlayerClickListener(TimeButton plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event){
		Block block = event.getClickedBlock();
		if (block == null)
			return;
		
		Player player = event.getPlayer();
		PlayerManager manager = plugin.getPlayerManager();
		PlayerAction action = manager.getActionForPlayer(player);
		TBEvent tbEvent = null;
		if (action.getActionType() == ActionType.ADD_BUTTON){ //Add a stone button event
			int clickCount = (Integer) action.getProperties().get("clickCount");
			int resetTime = (Integer) action.getProperties().get("resetTime");
			tbEvent = new TBAddButtonEvent(player, block, clickCount, resetTime);
		} else if (action.getActionType() == ActionType.REMOVE_BUTTON) { //Remove a stone button.
			tbEvent = new TBRemoveButtonEvent(block, player);
		} else if (ClickCountButton.isBlockCountedButton(block)) { //A stone button was clicked.
			tbEvent = new TBClickCountButtonEvent(player, block, event.getAction() == Action.LEFT_CLICK_BLOCK);
		} else if (block.getType() == Material.STONE_BUTTON) { //Notice that a stone button not a counted button is debug-event.
			tbEvent = new TBNoticeThatStoneButtonNotCountedButtonEvent(player);
		}
		
		if (tbEvent != null){
			tbEvent.execute();
			manager.setActionForPlayer(player, ActionType.NO_ACTION);
			if (tbEvent.isCancelled())
				event.setCancelled(true);
		}
	}
}
